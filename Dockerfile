FROM php:8.0-fpm-alpine

ENV COMPOSER_ALLOW_SUPERUSER 1
ENV COMPOSER_HOME /composer

COPY --from=composer:2.0 /usr/bin/composer /usr/bin/composer

RUN apk --no-cache update && \
    apk add --update --no-cache oniguruma-dev && \
    apk --no-cache add bash curl git make vim zip unzip openssl libzip-dev icu-dev && \
    docker-php-ext-install intl pdo_mysql bcmath


COPY ./containers/php/php.ini /usr/local/etc/php/php.ini

ENV APP_ROOT=/app
WORKDIR $APP_ROOT
